# 编写环境与使用方法

## 编写环境

1. **编辑器`VSCode`：** <https://code.visualstudio.com/>
1. **文档语言`Markdown`：**
    1. **`Markdown-Preview-Enhanced`官网：** <https://shd101wyy.github.io/markdown-preview-enhanced/#/>
    1. **`VSCode Markdown-Preview-Enhanced`插件：** <https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced>
1. **图示语言：**
    1. **`PlantUML`：**
        1. **`PlantUML`官网：** <https://plantuml.com/>
        1. **`VSCode PlantUML`插件：** <https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml>
    1. **`Graphviz`：**
        1. **`Graphviz`官网：** <https://www.graphviz.org/>
        1. **`Graphviz`下载地址：** <http://graphviz.org/download/>
        1. **`VSCode Graphviz`插件：** <https://marketplace.visualstudio.com/items?itemName=EFanZh.graphviz-preview>

:exclamation: 在`VSCode`中使用`PlantUML`和`Graphviz`需先安装`Graphviz`应用程序并将安装路径下的`bin`目录加入`Path`环境变量 :exclamation:

## 使用方法

### 方法一：使用markdown预览

可以通过使用`markdown`预览工具进行阅读，本仓库的`markdown`文件使用了[Markdown Preview Enhanced](https://shd101wyy.github.io/markdown-preview-enhanced/#/)（以下简称“`MPE`”）的一些扩展语法，因此建议使用该插件进行预览。

编写过程中使用的环境请详见[编写环境](./writing_env.md)。

### 方法二：使用已预导出的html文件

1. 使用`Chrome`或`Firefox`打开（:warning::exclamation:**`IE`无法正常打开**:exclamation::warning:），在`Windows`平台下，大多数情况`Firefox`阅读效果更佳（`Firefox`下`emoji`能完整显示，`Chrome`在有些机器上不能完整显示，具体原因未细究）
1. 可能需联网（`Coursewares`由于基于`revealjs`可能需要联网环境方能正常阅读）

:warning:由于自定义了`MPE`的`CSS`，因此，您本地使用`MPE`预览的结果可能与已预导出的`html文件`的显示效果不一样。

### 方法三：使用本地markdown预览后导出成PDF

由于`PDF`格式较大且为二进制文件，**本仓库不包含PDF格式的文件**，您可以自行在本地使用`MPE`（或其他`markdown`预览工具）导出成`PDF`。
